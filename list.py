# test-py: un ejemplo de Python

notas = {'Carlos':7,
	 'Ana':4,
	 'Paula':2,
	 'Luis':5,
	 'Javier':10,
	 'Maria':7,
	 'Miguel':8,
	}

print('Lista de suspensos')

for nombre, nota in notas.items():
	if (nota < 5):
		print(nombre, nota)
